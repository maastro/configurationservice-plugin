# Configuration Service Plugin

The Configuration Service Plugin is an autoconfiged Spring Boot Plugin for the MIA Configuration Service. It enables developers to build their own MIA Configuration Service to persist configuration within the [MIA framework](https://bitbucket.org/maastrosdt/binaries-and-documentation/wiki/Home).


## Prerequisites ##

- Java 8
- 512 MB RAM
- Modern browser 

## Creating a custom Configuration Service ##

In order to start creating your own configuration service for the MIA, setup a [Spring Boot](https://start.spring.io/) project and add the following dependency to your pom:

```xml
<dependency>
	<groupId>nl.maastro.mia.configurationserviceplugin</groupId>
	<artifactId>configurationserviceplugin</artifactId>
	<version>VERSION</version>
</dependency>
```

For an example on how to create a custom configuration service, see the [Universal Configuration Service](https://bitbucket.org/maastrosdt/universalconfigurationservice).