package nl.maastro.mia.configurationserviceplugin.web.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import nl.maastro.mia.configurationserviceplugin.service.ExtConfigFileService;
import nl.maastro.mia.configurationserviceplugin.web.util.HeaderUtil;

@RestController
@RequestMapping("/api/extconfig")
public class ExtConfigFileController {

    private final Logger logger = LoggerFactory.getLogger(ExtConfigFileController.class);
    
    @Autowired
    private ExtConfigFileService extConfigFileService;

    @GetMapping("/list")
    public List<String> listConfigurationFiles() throws IOException {
        return extConfigFileService.listFiles();
    }

    @GetMapping(value = "/file")
    public void getFile(@RequestParam("fileName") String fileName, HttpServletResponse response) throws IOException {
        logger.info("REST request to get configuration file: " + fileName);
        
        File file = extConfigFileService.getFile(fileName);
        
        response.setContentType("application/x-download");
        response.setHeader("Content-disposition", "attachment; filename=" + fileName);

        InputStream is = new FileInputStream(file);
        IOUtils.copy(is, response.getOutputStream());
        response.flushBuffer();
    }  
    
    @PostMapping(value = "/file")
    public ResponseEntity<String> addFile(@RequestParam("file") MultipartFile file) throws IOException {
        logger.info("REST request to save configuration file: " + file.getOriginalFilename());
        try {
            File savedFile = extConfigFileService.saveFile(file);
            String savedFileName = savedFile.getName();
            HttpHeaders responseHeader = new HttpHeaders();
            responseHeader.setContentType(MediaType.TEXT_PLAIN);
            return ResponseEntity.created(new URI("/api/extconfig/file/" + savedFileName))
                    .headers(responseHeader)
                    .body(savedFileName);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .headers(HeaderUtil.createFailureAlert("configFile", 
                            "uploadFailed", 
                            "Failed to upload configuration file"))
                    .body(null);
        }
    }
    
    @DeleteMapping(value = "/file")
    public ResponseEntity<Void> deleteFile(@RequestParam("fileName") String fileName) {
        logger.info("REST request to delete configuration file: " + fileName);
        try {
            extConfigFileService.deleteFile(fileName);
            return ResponseEntity.ok().build();
        } catch (FileNotFoundException e) {
            return ResponseEntity.badRequest()
                    .headers(HeaderUtil.createFailureAlert("configFile", "fileNotFound", "File does not exist"))
                    .build();
        }
    }
}
