package nl.maastro.mia.configurationserviceplugin.web.controller;

import java.net.URISyntaxException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import nl.maastro.mia.configurationserviceplugin.service.RtogService;


/**
 * REST controller for managing Rtog.
 */
@RestController
@RequestMapping("/api")
public class RtogController {

    private final Logger log = LoggerFactory.getLogger(RtogController.class);
        
    @Autowired
    private RtogService rtogService;
    
   
    @RequestMapping(value = "/rtogs",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<String>> getAllRtogs()
        throws URISyntaxException {
        log.debug("REST request to get a page of Rtogs");
        List<String> rtogs = rtogService.getList();
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<>(rtogs, headers, HttpStatus.OK);
    }

}
