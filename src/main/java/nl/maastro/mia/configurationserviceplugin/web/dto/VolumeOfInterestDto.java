package nl.maastro.mia.configurationserviceplugin.web.dto;

import java.util.ArrayList;
import java.util.List;

public class VolumeOfInterestDto {
	String name;
	List<String> rtogs = new ArrayList<>();
	List<String> operators = new ArrayList<>();
	boolean organAtRisk = true;
	
	public List<String> getRtogs() {
		return rtogs;
	}
	
	public void setRtogs(List<String> rtog) {
		this.rtogs = rtog;
	}
	
	public List<String> getOperators() {
		return operators;
	}
	
	public void setOperators(List<String> operators) {
		this.operators = operators;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public boolean isOrganAtRisk() {
		return organAtRisk;
	}

	public void setOrganAtRisk(boolean organAtRisk) {
		this.organAtRisk = organAtRisk;
	}
	
}