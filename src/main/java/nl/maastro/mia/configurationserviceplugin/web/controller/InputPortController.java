package nl.maastro.mia.configurationserviceplugin.web.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import nl.maastro.mia.configurationserviceplugin.entity.Configuration;
import nl.maastro.mia.configurationserviceplugin.repository.ConfigurationRepository;
import nl.maastro.mia.configurationserviceplugin.web.dto.InputPortDto;

/**
 * REST controller for managing ports.
 */
@RestController
@RequestMapping("/api")
public class InputPortController {

    private final Logger log = LoggerFactory.getLogger(InputPortController.class);

    private ConfigurationRepository configurationRepository;

    public InputPortController(ConfigurationRepository configurationRepository) {
        this.configurationRepository = configurationRepository;
    }

    @RequestMapping(value = "/inputports",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<InputPortDto> getConfiguredInputPorts() {
        log.debug("REST request to get the list of configured inputports");
        List<Configuration> configurations = configurationRepository.findAll();
        return configurations.stream().map(configuration -> convertToDto(configuration)).collect(Collectors.toList());
    }

    private InputPortDto convertToDto(Configuration configuration){
        InputPortDto inputPortDto = new InputPortDto();
        inputPortDto.setCreatePackages(configuration.getCreatePackages());
        inputPortDto.setDicomForward(configuration.getDicomForward());
        inputPortDto.setInputPort(configuration.getInputPort());
        return inputPortDto;
    }
}
