package nl.maastro.mia.configurationserviceplugin.web.dto;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ConfigurationDto {
	
    private Long id;
	private String name;
	private Integer inputPort;
	private Integer outputPort;
	private Boolean createPackages;
	private Boolean dicomForward;
	private Set<VolumeOfInterestDto> volumesOfInterest = new HashSet<>();
	private List<ModuleConfigurationDto> modulesConfiguration = new ArrayList<>();
	
	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getInputPort() {
		return inputPort;
	}

	public void setInputPort(Integer inputPort){
		this.inputPort = inputPort;
	}

	public Integer getOutputPort() {
		return outputPort;
	}

	public void setOutputPort(Integer outputPort) {
		this.outputPort = outputPort;
	}

	public Boolean getCreatePackages() {
		return createPackages;
	}

	public void setCreatePackages(Boolean createPackages) {
		this.createPackages = createPackages;
	}

	public Boolean getDicomForward() {
		return dicomForward;
	}

	public void setDicomForward(Boolean dicomForward) {
		this.dicomForward = dicomForward;
	}

	public Set<VolumeOfInterestDto> getVolumesOfInterest() {
		return volumesOfInterest;
	}

	public void setVolumesOfInterest(Set<VolumeOfInterestDto> volumesOfInterest) {
		this.volumesOfInterest = volumesOfInterest;
	}

	public List<ModuleConfigurationDto> getModulesConfiguration() {
		return modulesConfiguration;
	}

	public void setModulesConfiguration(List<ModuleConfigurationDto> modulesConfiguration) {
		this.modulesConfiguration = modulesConfiguration;
	}
	
}
