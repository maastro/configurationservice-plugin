package nl.maastro.mia.configurationserviceplugin.web;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class Advice extends ResponseEntityExceptionHandler {
    
    private static final Logger logger = LoggerFactory.getLogger(Advice.class);

    @ExceptionHandler(value = {IllegalArgumentException.class})
    protected ResponseEntity<Object> handleIllegalArgument(RuntimeException ex, WebRequest request) {
        String errorMessage = ex.getMessage();
        logger.error(errorMessage);
        return handleExceptionInternal(ex, errorMessage,
          new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(value = {IOException.class})
    protected ResponseEntity<Object> handleIo(Exception ex, WebRequest request) {
        String errorMessage = ex.getMessage();
        logger.error(errorMessage);
        return handleExceptionInternal(ex, errorMessage,
                new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }
}