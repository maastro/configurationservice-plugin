package nl.maastro.mia.configurationserviceplugin.web.dto;

public class InputPortDto {

    private Integer inputPort;
    private Boolean createPackages;
    private Boolean dicomForward;

    public Integer getInputPort() {
        return inputPort;
    }

    public void setInputPort(Integer inputPort) {
        this.inputPort = inputPort;
    }

    public Boolean getCreatePackages() {
        return createPackages;
    }

    public void setCreatePackages(Boolean createPackages) {
        this.createPackages = createPackages;
    }

    public Boolean getDicomForward() {
        return dicomForward;
    }

    public void setDicomForward(Boolean dicomForward) {
        this.dicomForward = dicomForward;
    }
}
