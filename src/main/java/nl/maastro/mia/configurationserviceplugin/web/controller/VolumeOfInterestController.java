package nl.maastro.mia.configurationserviceplugin.web.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import nl.maastro.mia.configurationserviceplugin.entity.Computation;
import nl.maastro.mia.configurationserviceplugin.entity.VolumeOfInterest;
import nl.maastro.mia.configurationserviceplugin.repository.ComputationRepository;
import nl.maastro.mia.configurationserviceplugin.repository.VolumeOfInterestRepository;
import nl.maastro.mia.configurationserviceplugin.web.util.HeaderUtil;
import nl.maastro.mia.configurationserviceplugin.web.util.PaginationUtil;

/**
 * REST controller for managing VolumeOfInterest.
 */
@RestController
@RequestMapping("/api")
public class VolumeOfInterestController {

    private final Logger logger = LoggerFactory.getLogger(VolumeOfInterestController.class);
        
    @Autowired
    private VolumeOfInterestRepository volumeOfInterestRepository;
    
    @Autowired
    private ComputationRepository computationRepository;
    
    /**
     * POST  /volume-of-interests : Create a new volumeOfInterest.
     *
     * @param volumeOfInterest the volumeOfInterest to create
     * @return the ResponseEntity with status 201 (Created) and with body the new volumeOfInterest, or with status 400 (Bad Request) if the volumeOfInterest has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/volume-of-interests",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<VolumeOfInterest> createVolumeOfInterest(@RequestBody VolumeOfInterest volumeOfInterest) throws URISyntaxException {
        logger.debug("REST request to save VolumeOfInterest : {}", volumeOfInterest);
        if (volumeOfInterest.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("volumeOfInterest", "idexists", "A new volumeOfInterest cannot already have an ID")).body(null);
        }
        VolumeOfInterest result = volumeOfInterestRepository.save(volumeOfInterest);
        return ResponseEntity.created(new URI("/api/volume-of-interests/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("volumeOfInterest", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /volume-of-interests : Updates an existing volumeOfInterest.
     *
     * @param volumeOfInterest the volumeOfInterest to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated volumeOfInterest,
     * or with status 400 (Bad Request) if the volumeOfInterest is not valid,
     * or with status 500 (Internal Server Error) if the volumeOfInterest couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/volume-of-interests",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<VolumeOfInterest> updateVolumeOfInterest(@RequestBody VolumeOfInterest volumeOfInterest) throws URISyntaxException {
        logger.debug("REST request to update VolumeOfInterest : {}", volumeOfInterest);
        if (volumeOfInterest.getId() == null) {
            return createVolumeOfInterest(volumeOfInterest);
        }
        VolumeOfInterest result = volumeOfInterestRepository.save(volumeOfInterest);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("volumeOfInterest", volumeOfInterest.getId().toString()))
            .body(result);
    }

    /**
     * GET  /volume-of-interests : get all the volumeOfInterests.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of volumeOfInterests in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/volume-of-interests",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<VolumeOfInterest>> getAllVolumeOfInterests(Pageable pageable)
        throws URISyntaxException {
        logger.debug("REST request to get a page of VolumeOfInterests");
        Page<VolumeOfInterest> page = volumeOfInterestRepository.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/volume-of-interests");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /volume-of-interests/:id : get the "id" volumeOfInterest.
     *
     * @param id the id of the volumeOfInterest to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the volumeOfInterest, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/volume-of-interests/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<VolumeOfInterest> getVolumeOfInterest(@PathVariable Long id) {
        logger.debug("REST request to get VolumeOfInterest : {}", id);
        VolumeOfInterest volumeOfInterest = volumeOfInterestRepository.getOne(id);
        return Optional.ofNullable(volumeOfInterest)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /volume-of-interests/:id : delete the "id" volumeOfInterest.
     *
     * @param id the id of the volumeOfInterest to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/volume-of-interests/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> deleteVolumeOfInterest(@PathVariable Long id) {
        logger.debug("REST request to delete VolumeOfInterest : {}", id);
        VolumeOfInterest voi = volumeOfInterestRepository.getOne(id);
        List<Computation> computations = computationRepository.findByVolumeOfInterest(voi);
        
        if(!computations.isEmpty()){
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("volumeOfInterest", "voiconstraintviolationexception", "Volume Of Interest is in use by computation(s)")).body(null);
        }
        volumeOfInterestRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("volumeOfInterest", id.toString())).build();
    }

}
