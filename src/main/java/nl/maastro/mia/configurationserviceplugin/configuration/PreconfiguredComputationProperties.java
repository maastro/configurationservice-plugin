package nl.maastro.mia.configurationserviceplugin.configuration;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;

import nl.maastro.mia.configurationserviceplugin.entity.Computation;

@ConfigurationProperties(prefix="preconfiguredcomputations")
public class PreconfiguredComputationProperties {
	
	private List<Computation> list = new ArrayList<>();
	
	public List<Computation> getList() {
		return list;
	}

	public void setList(List<Computation> list) {
		this.list = list;
	}
		
}