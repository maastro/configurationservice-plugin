package nl.maastro.mia.configurationserviceplugin.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
public class Configuration {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY) 
	private Long id;
	
	private String userId;
	
	private Integer inputPort;
	
	private String name;
	private Integer outputPort;
	
	@Column(nullable = true)
	private Boolean createPackages = true;
	
	@Column(nullable = true)
	private Boolean dicomForward = false;
	
	@ManyToMany
	@JoinTable(joinColumns=@JoinColumn(name = "CONFIGURATION_ID"),
		inverseJoinColumns=@JoinColumn(name="COMPUTATION_ID"))
	private List<Computation> computations = new ArrayList<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getInputPort() {
		return inputPort;
	}

	public void setInputPort(Integer inputPort) {
		this.inputPort = inputPort;
	}

	public Integer getOutputPort() {
		return outputPort;
	}

	public void setOutputPort(Integer outputPort) {
		this.outputPort = outputPort;
	}

	public List<Computation> getComputations() {
		return computations;
	}

	public void setComputations(List<Computation> computations) {
		this.computations = computations;
	}
	

	public Boolean getCreatePackages() {
		return createPackages;
	}

	public void setCreatePackages(Boolean createPackages) {
		this.createPackages = createPackages;
	}

	public Boolean getDicomForward() {
		return dicomForward;
	}

	public void setDicomForward(Boolean dicomForward) {
		this.dicomForward = dicomForward;
	}

	@Override
	public String toString() {
		return "Configuration [id=" + id + ", userId=" + userId + ", inputPort=" + inputPort + ", name=" + name
				+ ", outputPort=" + outputPort + ", computations=" + computations + "]";
	}

}
