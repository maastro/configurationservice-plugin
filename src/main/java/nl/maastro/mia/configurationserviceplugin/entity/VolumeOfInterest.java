package nl.maastro.mia.configurationserviceplugin.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import nl.maastro.mia.configurationserviceplugin.entity.enumeration.Operator;

@Entity
public class VolumeOfInterest {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY) 
	private Long id;
	
	@Column(unique=true)
	private String name;
	
	@ElementCollection
	private List<String> rtogs;
	
	@ElementCollection
	private List<Operator> operators;
	
	private boolean organAtRisk = true;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public List<Operator> getOperators() {
		return operators;
	}
	public void setOperators(List<Operator> operators) {
		this.operators = operators;
	}
	public List<String> getRtogs() {
		return rtogs;
	}
	public void setRtogs(List<String> rtogs) {
		this.rtogs = rtogs;
	}
	public boolean isOrganAtRisk() {
		return organAtRisk;
	}
	public void setOrganAtRisk(boolean organAtRisk) {
		this.organAtRisk = organAtRisk;
	}

}
