package nl.maastro.mia.configurationserviceplugin.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Computation {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY) 
	private Long id;
	
	private String computationIdentifier;
	
	private String moduleName;
	
	@ManyToOne
	private VolumeOfInterest volumeOfInterest;
	
	@Column(columnDefinition="text") 
	private String computationConfiguration;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getComputationIdentifier() {
		return computationIdentifier;
	}

	public void setComputationIdentifier(String computationIdentifier) {
		this.computationIdentifier = computationIdentifier;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public VolumeOfInterest getVolumeOfInterest() {
		return volumeOfInterest;
	}

	public void setVolumeOfInterest(VolumeOfInterest volumeOfInterest) {
		this.volumeOfInterest = volumeOfInterest;
	}

	public String getComputationConfiguration() {
		return computationConfiguration;
	}

	public void setComputationConfiguration(String computationConfiguration) {
		this.computationConfiguration = computationConfiguration;
	}

	@Override
	public String toString() {
		return "Computation [id=" + id + ", computationIdentifier=" + computationIdentifier + ", moduleName="
				+ moduleName + ", volumeOfInterest=" + volumeOfInterest + ", computationConfiguration="
				+ computationConfiguration + "]";
	}
	
}
