package nl.maastro.mia.configurationserviceplugin.entity.enumeration;

public enum Operator {
	PLUS, MINUS
}
