package nl.maastro.mia.configurationserviceplugin.autoconfigure;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import nl.maastro.mia.configurationserviceplugin.configuration.PreconfiguredComputationProperties;
import nl.maastro.mia.configurationserviceplugin.entity.Computation;
import nl.maastro.mia.configurationserviceplugin.repository.ComputationRepository;


@Configuration
@ComponentScan("nl.maastro.mia.configurationserviceplugin")
@EnableJpaRepositories("nl.maastro.mia.configurationserviceplugin")
@EntityScan("nl.maastro.mia.configurationserviceplugin")
@EnableConfigurationProperties(PreconfiguredComputationProperties.class)
public class ConfigurationServicePluginAutoConfiguration {
	
	@Autowired
	PreconfiguredComputationProperties preconfiguredComputationProperties;
	
	@Autowired
	ComputationRepository computationRepository;
	
	private static final Logger logger = LoggerFactory.getLogger(ConfigurationServicePluginAutoConfiguration.class);
	
	@PostConstruct
	public void initPreconfiguredComputationProperties() {
		
		logger.info("initPreconfiguredComputationProperties total nr: " + preconfiguredComputationProperties.getList().size());
		for (Computation computation : preconfiguredComputationProperties.getList()) {
			Computation existingComputation = computationRepository.findTopByComputationIdentifier(computation.getComputationIdentifier());
			if (existingComputation == null) {
				computationRepository.save(computation);
			} else {
				existingComputation.setModuleName(computation.getModuleName());
				existingComputation.setComputationConfiguration(computation.getComputationConfiguration());
				computationRepository.save(existingComputation);
			}
		}
	}

}