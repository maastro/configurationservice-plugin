package nl.maastro.mia.configurationserviceplugin.interfaces;

import org.springframework.stereotype.Service;

/**
 * This service should return a ConfigurationDto which will be set to the container
 */
@Service
public interface ContainerConfigurationServiceInterface {
	
	
}
