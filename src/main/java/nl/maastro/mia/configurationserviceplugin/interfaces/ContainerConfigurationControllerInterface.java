package nl.maastro.mia.configurationserviceplugin.interfaces;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * Implement a controller for the manager to get containerconfiguration
 */
@RestController
@RequestMapping("/api")
public interface ContainerConfigurationControllerInterface {

	
}
