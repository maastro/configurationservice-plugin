package nl.maastro.mia.configurationserviceplugin.interfaces;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Implement a controller to store manager configuration using the GUI
 */
@RestController
@RequestMapping("/api")
public interface GuiConfigurationControllerInterface {

}
