package nl.maastro.mia.configurationserviceplugin.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import nl.maastro.mia.configurationserviceplugin.entity.Computation;
import nl.maastro.mia.configurationserviceplugin.entity.Configuration;

@Repository
public interface ConfigurationRepository extends JpaRepository<Configuration, Long>{

	Configuration findTop1ByInputPort(Integer inputPort);
	List<Configuration> findByInputPort(Integer inputPort);
	
	List<Configuration> findAllByUserId(String userId);
	List<Configuration> findByComputations(Computation computation);
}
