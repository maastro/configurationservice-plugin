package nl.maastro.mia.configurationserviceplugin.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import nl.maastro.mia.configurationserviceplugin.entity.VolumeOfInterest;

@Repository
public interface VolumeOfInterestRepository extends JpaRepository<VolumeOfInterest, Long> {

}
