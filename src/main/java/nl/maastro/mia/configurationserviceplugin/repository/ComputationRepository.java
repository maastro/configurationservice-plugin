package nl.maastro.mia.configurationserviceplugin.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import nl.maastro.mia.configurationserviceplugin.entity.Computation;
import nl.maastro.mia.configurationserviceplugin.entity.VolumeOfInterest;

@Repository
public interface ComputationRepository extends JpaRepository<Computation, Long> {

	Computation findTopByComputationIdentifier(String computationIdentifier);
	List<Computation> findByVolumeOfInterest(VolumeOfInterest volumeOfInterest);

}
