package nl.maastro.mia.configurationserviceplugin.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Service;

import nl.maastro.mia.configurationserviceplugin.entity.Computation;
import nl.maastro.mia.configurationserviceplugin.entity.VolumeOfInterest;
import nl.maastro.mia.configurationserviceplugin.entity.enumeration.Operator;
import nl.maastro.mia.configurationserviceplugin.web.dto.VolumeOfInterestDto;

@Service
public class VolumeOfInterestService {
	
	
	public Set<VolumeOfInterestDto> getVolumesOfInterestDto(List<Computation> comptutations){
		Set<VolumeOfInterestDto> set = new HashSet<>();
		for (Computation entity : comptutations){
			set.add(mapVolumeOfInterest(entity.getVolumeOfInterest()));
		}
		return set;
	}
	
	public VolumeOfInterestDto mapVolumeOfInterest(VolumeOfInterest entity){
		if(entity==null)
			return null;
		
		VolumeOfInterestDto dto = new VolumeOfInterestDto();
		dto.setName(entity.getName());
		dto.setOperators(mapOperators(entity.getOperators()));
		dto.setRtogs(entity.getRtogs());
		dto.setOrganAtRisk(dto.isOrganAtRisk());
		return dto;
	}
	
	private List<String> mapOperators(List<Operator> operators){
		List<String> list = new ArrayList<>();
		for(Operator operator : operators){
			
		    switch (operator) {
		        case PLUS:
		        	list.add("+");
		            break;
		        case MINUS:
		            list.add("-");
		            break;
		    }
		    
		}
		return list;
	}
	
}
