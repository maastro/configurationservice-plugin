package nl.maastro.mia.configurationserviceplugin.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.naming.ConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@ConfigurationProperties(prefix="moduleconfiguration")
public class RequiredModalityService{
	

	private final static Logger logger = LoggerFactory.getLogger(RequiredModalityService.class);

	private RestTemplate restTemplate;

	private Map<String,Set<String>> requiredModuleModalities = new HashMap<>();

	private List<String> workers = new ArrayList<>();

	public RequiredModalityService(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}


	public Set<String> getRequiredModalities(String moduleName) throws ConfigurationException {
		if(requiredModuleModalities.keySet().isEmpty() || !requiredModuleModalities.containsKey(moduleName))
			setRequiredModalities();

		if(requiredModuleModalities.containsKey(moduleName))
			return new HashSet<>(requiredModuleModalities.get(moduleName));
		else
			throw new ConfigurationException("Unable to determine requiredModalities for moduleName:" + moduleName);
	}

	private void setRequiredModalities(){
		for(String worker : workers){
			setRequiredModalities(worker);
		}
	}

	private void setRequiredModalities(String worker){
	    logger.debug("Retrieving required modalities from worker: " + worker);
		String url = "http://" + worker + "/api/requiredmodalities";
		try{
			@SuppressWarnings("unchecked")
			Map<String,Set<String>> response = restTemplate.getForObject(url, Map.class);
			if(response == null){
				logger.warn("RequiredModalities query returned null for: " +url);
				return;
			}
			requiredModuleModalities.putAll(response);
		}
		catch(Exception e){
		    logger.error("Unable to retrieve required modalities from worker: " + worker, e);
		}
	}

	public List<String> getWorkers() {
		return workers;
	}

	public void setWorkers(List<String> workers) {
		this.workers = workers;
	}
}
