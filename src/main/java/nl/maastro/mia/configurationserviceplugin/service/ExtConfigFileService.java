package nl.maastro.mia.configurationserviceplugin.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class ExtConfigFileService {

    private static final Logger logger = LoggerFactory.getLogger(ExtConfigFileService.class);

    private static final String extConfigDir = "./extconfig";
    
    @PostConstruct
    private void createExtConfigDirectory() throws IOException {
        logger.info("Directory for external config:" + Paths.get(extConfigDir).toAbsolutePath());
        if(!Files.isDirectory(Paths.get(extConfigDir)))
            Files.createDirectory(Paths.get(extConfigDir));
    }

    public List<String> listFiles() throws IOException {
        return Files.list(Paths.get(extConfigDir))
                .map(path -> path.getFileName().toString())
                .collect(Collectors.toList()); 
    }
    
    public File getFile(String fileName) throws FileNotFoundException {
        File file = new File(extConfigDir + "/" + fileName);
        if (file.exists()) {
            return file;
        } else {
            throw new FileNotFoundException("File does not exist: " + fileName);
        }
    }
    
    public void deleteFile(String fileName) throws FileNotFoundException {
        File file = getFile(fileName);
        file.delete();
    }
    
    public File saveFile(MultipartFile multipartFile) throws Exception {
        String extension = getFileExtension(multipartFile.getOriginalFilename());
        String newFileName = UUID.randomUUID() + "." + extension;
        Path newFilePath = Paths.get(extConfigDir, newFileName).toAbsolutePath().normalize();
        File newFile = new File(newFilePath.toString());
        FileOutputStream outputStream = new FileOutputStream(newFile);
        try {
            outputStream.write(multipartFile.getBytes());
            logger.info("External configuration file saved to: " + newFilePath.toString());
            return newFile;
        } catch (Exception e) {
            logger.error("Failed to save external configuration file: " + multipartFile.getOriginalFilename());
            throw e;
        } finally {
            outputStream.close();
        }
    }

    private String getFileExtension(String fileName) {
        int index = fileName.lastIndexOf('.');
        if (index >= 0) {
            return fileName.substring(index + 1);
        } else {
            return "";
        }
    }
}
